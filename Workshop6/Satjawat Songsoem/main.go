package main

import (
	"context"
	"fmt"
	"gofiber/handler"
	"gofiber/repository"
	"gofiber/service"
	"net/http"

	"log"
	"time"

	"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/cors"
	"github.com/spf13/viper"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"gorm.io/gorm/logger"
)

var db *gorm.DB

func initConfig() {
	viper.SetConfigName("config")
	viper.SetConfigType("yaml")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(err)
	}
}

type SqlLogger struct {
	logger.Interface
}

func (l SqlLogger) Trace(ctx context.Context, begin time.Time, fc func() (sql string, rowsAffected int64), err error) {
	sql, _ := fc()
	fmt.Printf("%v\n=============================\n", sql)
}

func initDatabase() *gorm.DB {
	var err error
	initConfig()
	dsn := fmt.Sprintf("%v:%v@tcp(%v:%v)/%v?parseTime=true",
		viper.GetString("db.username"),
		viper.GetString("db.password"),
		viper.GetString("db.host"),
		viper.GetInt("db.port"),
		viper.GetString("db.database"),
	)
	dial := mysql.Open(dsn)
	db, err := gorm.Open(dial, &gorm.Config{
		Logger: &SqlLogger{},
		DryRun: false})
	if err != nil {
		log.Fatal(err)
	}
	fmt.Println(db)
	return db
}

func initRedis() *redis.Client {
	return redis.NewClient(&redis.Options{
		Addr: "localhost:6379",
	})
}


func main() {
	db = initDatabase()
	redisClient := initRedis()
	_ = redisClient

	app := fiber.New()

	app.Use(cors.New(cors.Config{
		AllowOrigins: "*",
		AllowMethods: "GET,POST,PUT,DELETE",
		AllowHeaders: "Accept, Content-Type, Content-Length, Accept-Encoding, Authorization,X-CSRF-Token",
	}))

	customerRepositoryDB := repository.NewCustomerRepositoryDB(db)
	customerService := service.NewCustomerService(customerRepositoryDB)
	customerHandler := handler.NewCustomerHandlerRedis(customerService, redisClient)

	//app.Get("/customer/:customerID", customerHandler.GetCustomer)
	app.Get("/customer", customerHandler.GetCustomers)
	//app.Post("/customer", customerHandler.InsertCustomer)
	//app.Put("/customer", customerHandler.UpdateCustomer)
	//app.Delete("/customer/:customerID", customerHandler.RemoveCustomer)

	app.Listen(":8000")
}

func init() {

	hystrix.ConfigureCommand("customer", hystrix.CommandConfig{
		Timeout:                500,
		RequestVolumeThreshold: 1,
		ErrorPercentThreshold:  100,
		SleepWindow:            15000,
	})

	hystrixStreamHandler := hystrix.NewStreamHandler()
	hystrixStreamHandler.Start()
	go http.ListenAndServe(":8001", hystrixStreamHandler)

}