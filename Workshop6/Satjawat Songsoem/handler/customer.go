package handler

import "github.com/gofiber/fiber/v2"

type CustomerHandler interface {
	GetCustomers(c *fiber.Ctx) error
}