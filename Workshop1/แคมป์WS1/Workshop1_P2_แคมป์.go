package main

import "fmt"

func fibonacci(num int) {
	a := 0
	b := 1
	c := b
	fmt.Printf("%d %d", a, b)
	for {
		c = b
		b = a + b
		if b >= num {

			break
		}
		a = c
		fmt.Printf(" %d", b)
	}
}

func main() {
	fibonacci(50)
}
