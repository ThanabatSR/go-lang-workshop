package main

import "fmt"

func main() {
    var TimesTable [12][12]int

    // Populate the table with products
    for i := 0; i < 12; i++ {
        for j := 0; j < 12; j++ {
            TimesTable[i][j] = (i + 1) * (j + 1)
			fmt.Printf("%d\t", TimesTable[i][j])
        }
		fmt.Println()
    }

}