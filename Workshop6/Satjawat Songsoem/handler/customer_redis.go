package handler

import (
	"context"
	"encoding/json"
	"fmt"
	"gofiber/service"
	"time"

	//"github.com/afex/hystrix-go/hystrix"
	"github.com/go-redis/redis/v8"
	"github.com/gofiber/fiber/v2"
)

type customerHandlerRedis struct {
	custSrv service.CustomerService
	redisClient *redis.Client
}

func NewCustomerHandlerRedis(custSrv service.CustomerService, redisClient *redis.Client) CustomerHandler {
    return customerHandlerRedis{custSrv, redisClient}
}

func (h customerHandlerRedis) GetCustomers(c *fiber.Ctx) error {
	
	//output := make(chan string, 1)
	//hystrix.Go("customer", func() error {
		key := "handler::GetCustomers"
	

	// Redis GET
	if responseJson, err := h.redisClient.Get(context.Background(), key).Result(); err == nil {
		fmt.Println("redis")
		c.Set("Content-Type", "application/json")
		return c.SendString(responseJson)
	}

	// Service
	customers, err := h.custSrv.GetCustomers()
	if err != nil {
		return err
	}

	response := fiber.Map{
		"status":   "ok",
		"products": customers,
	}

	// Redis SET
	if data, err := json.Marshal(response); err == nil {
		h.redisClient.Set(context.Background(), key, string(data), time.Second*10)
	}

	fmt.Println("database")
	return c.JSON(response)
	
	}/*, func(err error) error {
		fmt.Println(err)

		return err
	})
	out := <-output
	return c.SendString(out)
}*/