package repository

type Customer struct {
	CustomerID  int    `db:"customer_id"`
	Name        string `db:"name"`
	DateCreate  string `db:"date_create"`
	PhoneNumber string `db:"phone_number"`
}

type CustomerRepository interface {
    GetAll() ([]Customer, error)
    GetById(id int) (*Customer, error)
    DeleteCustomer(id int) error
    InsertCustomer(c Customer) (*Customer, error)
    UpdateCustomer(id int, c Customer) (*Customer, error)
}

