package main

import "fmt"

type product struct {
	productName string
	price       float64
	weight      float64
}

func main() {
	product1 := product{
		productName: "iPhone 13",
		price:       26900,
		weight:      0.21,
	}
	fmt.Println("Product name: ", product1.productName)
	fmt.Println("Price: ", product1.price)
	fmt.Println("Weight: ", product1.weight, "kg")
}
